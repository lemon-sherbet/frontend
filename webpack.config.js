const BUILD_DIR = __dirname + "/output/";
const SRC_DIR = __dirname + "/src/";
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MinifyPlugin = require("babel-minify-webpack-plugin");
const ExtractTextPlugin = require("extract-text-webpack-plugin");
const extractCSS = new ExtractTextPlugin("assets/[chunkhash].css");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const path = require("path");
const config = require("./config.json");
const fs = require("fs");
const ScriptExtHtmlWebpackPlugin = require("script-ext-html-webpack-plugin");
const HtmlWebpackInlineSVGPlugin = require("html-webpack-inline-svg-plugin");

const pageList = [
  {
    title: config.SERVICE_NAME,
    chunkName: "index"
  },
  {
    title: `Login | ${config.SERVICE_NAME}`,
    chunkName: "login"
  },
  {
    title: `Upload | ${config.SERVICE_NAME}`,
    chunkName: "upload"
  },
  {
    title: `My Account | ${config.SERVICE_NAME}`,
    chunkName: "account"
  },
  {
    title: `Uploader Settings | ${config.SERVICE_NAME}`,
    chunkName: "uploader",
    chunks: ["accountSelect2"]
  },
  {
    title: "Logout",
    chunkName: "logout"
  },
  {
    title: `New Token | ${config.SERVICE_NAME}`,
    chunkName: "token"
  },
  {
    title: `About Us | ${config.SERVICE_NAME}`,
    chunkName: "about"
  },
  {
    title: `FAQ | ${config.SERVICE_NAME}`,
    chunkName: "faq"
  },
  {
    title: `My Files | ${config.SERVICE_NAME}`,
    chunkName: "list"
  },
  {
    title: `My Shortened URLs | ${config.SERVICE_NAME}`,
    chunkName: "shortlist"
  },
  {
    title: `Shorten | ${config.SERVICE_NAME}`,
    chunkName: "shorten"
  },
  {
    title: `Signup | ${config.SERVICE_NAME}`,
    chunkName: "signup"
  },
  {
    title: `Admin | ${config.SERVICE_NAME}`,
    chunkName: "admin",
    overrideName: "admoon"
  },
  {
    title: `Delete your Account | ${config.SERVICE_NAME}`,
    chunkName: "deleteconfirm"
  },
  {
    title: `Password Reset | ${config.SERVICE_NAME}`,
    chunkName: "password_reset"
  },
  {
    title: `Album | ${config.SERVICE_NAME}`,
    chunkName: "album"
  },
  {
    title: `404 | ${config.SERVICE_NAME}`,
    chunkName: "404"
  },
  ...(config.PRIVACY_POLICY
    ? [
        {
          title: `Privacy Policy | ${config.SERVICE_NAME}`,
          chunkName: "privacy",
          noChunkJS: true
        }
      ]
    : [])
];

const entryPointNames = [
  "login",
  "upload",
  "account",
  "uploader",
  "logout",
  "token",
  "theme",
  "themeCSS",
  "about",
  "faq",
  "list",
  "shortlist",
  "shorten",
  "signup",
  "admin",
  "deleteconfirm",
  "password_reset",
  "album",
  "clientInsert",
  "404"
];
let entry = {};
for (const name of entryPointNames) {
  entry[name] = `${SRC_DIR}js/${name}.js`;
}

module.exports = {
  entry: {
    babelPolyfill: "babel-polyfill",
    index: `${SRC_DIR}js/homepage.js`,
    bootstrapJs: "bootstrap",
    themeCSS: `${SRC_DIR}js/themeCSS.js`,
    accountSelect2: `${SRC_DIR}js/account-select2`,
    ...entry
  },
  resolve: {
    alias: {
      // import "@/file" goes to "src/file"
      "@": path.resolve(SRC_DIR)
    }
  },
  output: {
    filename: "assets/[name].[chunkhash].js",
    path: BUILD_DIR,
    chunkFilename: "assets/[name].[chunkhash].js",
    sourceMapFilename: "assets/[name].[chunkhash].map.js",
    publicPath: "/"
  },
  plugins: [
    extractCSS,
    ...(process.env.NODE_ENV == "production"
      ? [
          new CleanWebpackPlugin([BUILD_DIR], {
            beforeEmit: true
          }),
          new MinifyPlugin(
            {
              // removeConsole: true,
              // removeDebugger: true
            },
            {
              comments: false
            }
          )
        ]
      : []),
    ...pageList.map(
      page =>
        new HtmlWebpackPlugin({
          title: page.title,
          filename: `${page.overrideName || page.chunkName}.html`,
          template: `${SRC_DIR}/${page.chunkName}.pug`,
          chunks: [
            "themeCSS",
            "babelPolyfill",
            "clientInsert",
            "theme",
            ...(page.noChunkJS ? [] : [page.chunkName]),
            "bootstrapJs"
          ].concat(page.chunks || []),
          mobile: true,
          inject: true,
          minify: {
            collapseWhitespace: true,
            removeComments: true,
            removeAttributeQuotes: true
          },
          xhtml: true
        })
    ),
    new ScriptExtHtmlWebpackPlugin({
      sync: ["babelPolyfill", "clientInsert"],
      defaultAttribute: "async"
    }),
    new HtmlWebpackInlineSVGPlugin()
  ],
  module: {
    rules: [
      {
        test: /\.js$/,
        include: [SRC_DIR, `${SRC_DIR}/js`],
        loader: "babel-loader",
        options: {
          babelrc: false,
          presets: [
            [
              "@babel/preset-env",
              {
                targets: {
                  browsers: [
                    ">0.25%", // Only support browsers with >0.25% market share
                    "not ie 11", // Ignore IE 11
                    "not op_mini all" // Ignore Opera Mini
                  ]
                }
              }
            ]
          ],
          plugins: ["@babel/plugin-syntax-dynamic-import"]
        }
      },
      {
        test: /\.s?css$/,
        use: extractCSS.extract([
          {
            loader: "css-loader",
            options: { minimize: process.env.NODE_ENV == "production" }
          },

          {
            loader: "sass-loader"
          },
          {
            loader: "prepend-loader",
            options: {
              data: `@import "~bootswatch/dist/darkly/variables";
@import "~bootstrap/scss/functions";
@import "~bootstrap/scss/variables";
@import "~@/styles/vars.scss";
`
            }
          }
        ])
      },
      {
        test: /\.pug$/,
        loader: "pug-loader",
        query: {},
        exclude: /node_modules/
      },
      {
        test: /\.(?:(?:sv|pn)g|webm)$/,
        exclude: /momo\d{1,2}\.png/,
        use: ["file-loader?name=./assets/[hash].[ext]"]
      }
    ]
  }
};
