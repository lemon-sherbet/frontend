import common from "./commonCode.js";
import onLoad from "./onload.js";
import config from "@/../config.json";

const authenticatedPages = [
  "/upload.html",
  "/logout.html",
  "/account.html",
  "/list.html",
  "/shortlist.html",
  "/shorten.html",
  "/admoon.html",
  "/uploader.html"
];

const adminPages = ["/admoon.html"];

onLoad(async function() {
  console.log("Dom loaded");

  const now = new Date();
  if (now.getMonth() == 3 && now.getDate() == 1)
    document.firstElementChild.classList += " delicious";

  const acc = document.getElementById("nav-account");
  const navLinks = document.getElementsByClassName("intellectual-link");
  for (const navLink of navLinks) {
    const targetURL = navLink.href;
    // Strip hash param from the url because otherwise we reload the page if we are loaded with a hashparam
    const lastIndex = window.location.href.lastIndexOf("#");
    if (
      window.location.href.substring(
        0,
        lastIndex == -1 ? undefined : lastIndex
      ) == targetURL
    ) {
      if (navLink.classList.contains("nav-link"))
        navLink.parentNode.classList += " active";
      navLink.href = "#";
    }
  }

  const profileDisplay = document.getElementById("profile-container");
  const profileCache = JSON.parse(
    window.localStorage.getItem("profile-cache") || null
  );
  if (profileCache) isLoggedIn(profileCache);
  window.client.profile = await profilePromise;
  console.log(window.client.profile);
  function isLoggedIn(profile) {
    acc.innerText = profile.username;
    document.body.classList += " logged-in";
    if (profile.admin) {
      document.body.classList += " is-admin";
    }
  }
  if (!window.client.profile) {
    window.localStorage.removeItem("profile-cache");
    acc.innerText = "Account";
    document.body.classList = document.body.classList
      .toString()
      .replace("logged-in", "")
      .replace("is-admin", "");
  }
  if (window.client.profile) {
    isLoggedIn(window.client.profile);
    window.localStorage.setItem(
      "profile-cache",
      JSON.stringify(window.client.profile)
    );
  } else if (authenticatedPages.includes(window.location.pathname)) {
    // Hash param is used to know where to redirect back to after login.
    document.getElementById("garfield-login").href = `/login.html#${
      window.location.pathname
    }`;
    document.body.classList += " show-garfield";
    document.title = "Access Denied | Elixire";
  }

  if (
    window.client.profile &&
    !window.client.profile.admin &&
    adminPages.includes(window.location.pathname)
  ) {
    document.getElementById("garfield-login").href = `/login.html#${
      window.location.pathname
    }`;
    document.body.classList += " show-garfield";
    document.title = "Access Denied | Elixire";
  }

  const gdprFuckJquery = document.getElementById("open-gdpr-modal");
  if (
    window.client.profile &&
    (window.client.profile.consented === null ||
      window.location.hash == "#ANNOY_ME_TO_DEATH")
  ) {
    gdprFuckJquery.click();
  }
  const acceptBtn = document.getElementById("gdpr-btn");
  // const denyBtn = document.getElementById("gdpr-deny");
  const deleteBtn = document.getElementById("gdpr-delete");
  const password = document.getElementById("gdpr-password");
  const passwordReset = document.getElementById("gdpr-reset");
  const form = document.getElementById("gdpr-form");
  const dismissBtn = document.getElementById("dismiss-modal");
  const dataProcessing = document.getElementById("gdpr-minimal");
  password.addEventListener("keypress", function(ev) {
    if (ev.key == "Enter") {
      ev.preventDefault();
      ev.stopPropagation();
      submitGdpr(true);
      return false;
    }
  });
  acceptBtn.addEventListener("click", () => submitGdpr(dataProcessing.checked));

  deleteBtn.addEventListener("click", async function() {
    try {
      // if (password.value.length < 8 || password.value.length > 100) {
      //   throw { userMessage: "Invalid password" };
      // }
      await client.deleteAccount(password.value);
      alert(
        "Sorry to see you go, check your email for a verification link... :("
      );
    } catch (err) {
      password.setCustomValidity(err.userMessage || err.message);
      form.classList = "needs-validation was-validated logged-in-only";
    }
  });

  passwordReset.addEventListener("click", async function() {
    await client.resetPassword(client.profile.username);
    alert("Check your email");
  });

  async function submitGdpr(allowed) {
    if (window.client.profile) {
      try {
        // if (password.value.length < 8 || password.value.length > 100) {
        //   throw new Error("BAD_AUTH");
        // }
        await client.updateAccount({
          password: password.value,
          consented: allowed
        });
      } catch (err) {
        form.classList = "was-validated needs-validation logged-in-only";
        return password.setCustomValidity(err.userMessage || err.message);
      }
    }
    window.localStorage.setItem("gdpr-consent", allowed);
    gdprFuckJquery.click();
  }

  const allowedHostnames = ["localhost", "0.0.0.0", "127.0.0.1"];
  if (
    !config.HIDE_NOTICE &&
    !allowedHostnames.includes(window.location.hostname)
  )
    common.sendAlert(
      "warning",
      'This instance of Elixire is not owned by developers of this software, the information may be incorrect. (Note to instance owners, set "HIDE_NOTICE" to true in frontend/config.json to hide this once you have corrected any discrepencies)'
    );

  // Show kitty.......

  document.addEventListener("keydown", function(e) {
    if (e.shiftKey && e.keyCode == 113 && !window.imageShower) {
      // here's our cue
      window.imageShower = function(posts) {
        window.imageShower = null;
        if (showImageBlock) clearTimeout(showImageBlock);
        const post = posts[0];
        const image = document.createElement("img");
        image.src = post.file_url;
        image.style.position = "absolute";
        image.style.top = 0;
        image.style.bottom = 0;
        image.style.left = 0;
        image.style.right = 0;
        image.style.maxWidth = "100%";
        image.style.maxHeight = "100%";
        image.style.margin = "auto";
        image.addEventListener("click", function() {
          image.remove();
        });
        document.body.appendChild(image);
      };
      let showImageBlock = null;
      function askForImage() {
        const script = document.createElement("script");
        script.src = `https://e621.net/post/index.json?limit=1&tags=catgirl%20order:random%20rating:safe&callback=window.imageShower&_=${new Date().getTime()}`;
        document.body.appendChild(script);
        // Keep retrying until it loads
        showImageBlock = setTimeout(askForImage, 2000);
      }
      askForImage();
    }
  });
});
window.addEventListener("error", function(event) {
  // oopsie woopsie
  console.log(event);
  document.body.classList += " error-thrown";
  const errorInfo = document.getElementById("error-info");
  errorInfo.innerText = event.message;
  const filename = document.getElementById("filename");
  filename.innerText = `${event.filename}:${event.lineno}:${event.colno}`;
});
